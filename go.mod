module github.com/moby/term

go 1.18

require (
	github.com/Azure/go-ansiterm v0.0.0-20210617225240-d185dfc1b5a1
	github.com/creack/pty v1.1.11
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22
)
